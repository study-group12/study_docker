init: down up-and-build

up:
	docker compose up -d
up-and-build:
	docker compose up -d --build
down:
	docker compose down
restart:
	docker compose restart
clear:
	docker compose down -v
php:
	docker compose exec php bash
mysql:
	docker compose exec db bash